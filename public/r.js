function bin2hex(s) {
  // From: http://phpjs.org/functions
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Onno Marsman
  // +   bugfixed by: Linuxworld
  // +   improved by: ntoniazzi (http://phpjs.org/functions/bin2hex:361#comment_177616)
  // *     example 1: bin2hex('Kev');
  // *     returns 1: '4b6576'
  // *     example 2: bin2hex(String.fromCharCode(0x00));
  // *     returns 2: '00'

  var i, l, o = "",
    n;

  s += "";

  for (i = 0, l = s.length; i < l; i++) {
    n = s.charCodeAt(i).toString(16)
    o += n.length < 2 ? "0" + n : n;
  }

  return o;
}

var startTime = Date.now()

var canvas = document.createElement('canvas')
var ctx = canvas.getContext("2d")
ctx.font = "28px sans"
ctx.fillStyle = '#FF0000'
ctx.fillRect(25, 25, 100, 50)
ctx.fillStyle = '#000000'
ctx.fillText("BMI 11 Ruky", 10, 50)

var b64 = canvas.toDataURL().toString().replace("data:image/png;base64,", "")
var bin = atob(b64)
var crc = bin2hex(bin.slice(-36, -12))

var appName = window.navigator.appName
var platform = window.navigator.platform
var userAgent = window.navigator.userAgent
var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
var screenHeight = window.screen.height
var screenWidth = window.screen.width
var pdensity = window.devicePixelRatio
var ref = document.referrer
var url = document.location

var form = new FormData()
form.append('hash', crc)
form.append('appName' , appName)
form.append('platform', platform)
form.append('userAgent', userAgent)
form.append('timezone', timeZone)
form.append('screenHeight', screenHeight)
form.append('screenWidth', screenWidth)
form.append('dpr', pdensity)
form.append('ref', ref)
form.append('url', url)

fetch('https://ruky.me/tracking/', {
  method: 'POST',
  body: form
})

window.addEventListener('onunload',()=>{
  var endTime = Date.now()
  var timeSpent = endTime - startTime
  fetch(`https://ruky.me/tracking?mode=time&hash=${crc}&spent=${timeSpent}`, {
    mode: 'no-cors'
  })
})
window.dispatchEvent(new Event('onunload'))
